// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();

	FInputModeGameOnly InputMode;
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetInputMode(InputMode);
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandleVerticalPlayerInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandleHorizontalPlayerInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandleVerticalPlayerInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastDirection!=EMovementDirection::DOWN)
		{
			SnakeActor->CurrentDirection = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeActor->LastDirection != EMovementDirection::UP)
		{
			SnakeActor->CurrentDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandleHorizontalPlayerInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastDirection != EMovementDirection::LEFT)
		{
			SnakeActor->CurrentDirection = EMovementDirection::RIGHT;
		}
		else if (value < 0 && SnakeActor->LastDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->CurrentDirection = EMovementDirection::LEFT;
		}
	}
}

