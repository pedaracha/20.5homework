// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBaseElement.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"

// Sets default values
ASnakeBaseElement::ASnakeBaseElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ASnakeBaseElement::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASnakeBaseElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeBaseElement::SetFirstElementType_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeBaseElement::HandleBeginOverlap);
}

void ASnakeBaseElement::Interact(AActor* interactor,bool IsHead)
{
	auto snake = Cast<ASnakeBase>(interactor);
	if (IsValid(snake))
	{
		snake->Destroy();
	}
}

void ASnakeBaseElement::HandleBeginOverlap(UPrimitiveComponent* OverlapComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this,OtherActor);
	}
}

void ASnakeBaseElement::ToggleCollicion()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::QueryOnly)
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
	else
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
}
