// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeBaseElement.h"
#include "Interractable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.0f;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	Init(4);
	SetActorTickInterval(Speed);
	CurrentDirection = EMovementDirection::DOWN;
	LastDirection = CurrentDirection;
}

void ASnakeBase::Init(int ElementsNum)
{
	//Create head
	FVector NewLocation(GetActorLocation());
	FTransform NewTransform(NewLocation);
	auto NewSnakeElem = GetWorld()->SpawnActor<ASnakeBaseElement>(SnakeElementClass, NewTransform);
	NewSnakeElem->SnakeOwner = this;
	SnakeElements.Add(NewSnakeElem);
	NewSnakeElem->SetFirstElementType();

	//Create body
	for (int i = 1;i < ElementsNum;i++)
	{
		AddSnakeElement();
	}
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement()
{
	FVector NewLocation(SnakeElements.Last()->GetActorLocation());
	FTransform NewTransform(NewLocation);
	auto NewSnakeElem = GetWorld()->SpawnActor<ASnakeBaseElement>(SnakeElementClass, NewTransform);
	NewSnakeElem->SnakeOwner = this;
	SnakeElements.Add(NewSnakeElem);
}

void ASnakeBase::RemoveSnakeElement()
{
	if (SnakeElements.Num() > 2) 
	{
		int32 last = SnakeElements.Num() - 1;
		SnakeElements[last]->Destroy();
		SnakeElements.RemoveAt(last);
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(0,0,0);

	switch(CurrentDirection)
	{
		case EMovementDirection::UP:
			MovementVector.X = ElementSize;
			break;
		case EMovementDirection::DOWN:
			MovementVector.X = -ElementSize;
			break;
		case EMovementDirection::LEFT:
			MovementVector.Y = -ElementSize;
			break;
		case EMovementDirection::RIGHT:
			MovementVector.Y = ElementSize;
			break;
	}

	SnakeElements[0]->ToggleCollicion();
	for (int i = SnakeElements.Num() - 1;i > 0;i--) {
		auto currentElement = SnakeElements[i];
		auto prevElement = SnakeElements[i - 1];
		auto newPos = prevElement->GetActorLocation();
		currentElement->SetActorLocation(newPos);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollicion();

	LastDirection = CurrentDirection;
}

void ASnakeBase::SnakeElementOverlap(ASnakeBaseElement* OverlapedElement,AActor* Other)
{
	if (IsValid(OverlapedElement))
	{
		int32 index;
		SnakeElements.Find(OverlapedElement, index);
		bool bIsFirst = index == 0;
		IInterractable* InteractableInterface = Cast<IInterractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this,bIsFirst);
		}
	}
}

