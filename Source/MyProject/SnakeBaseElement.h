// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interractable.h"
#include "SnakeBaseElement.generated.h"

class UStaticMeshComponent;
class ASnakeBase;

UCLASS()
class MYPROJECT_API ASnakeBaseElement : public AActor,public IInterractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBaseElement();

	UPROPERTY(VisibleAnyWhere, BlueprintReadonly)
		UStaticMeshComponent* MeshComponent;

	UPROPERTY()
		ASnakeBase* SnakeOwner;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
		void SetFirstElementType();
	void SetFirstElementType_Implementation();

	void Interact(AActor* interactor,bool IsHead) override;

	UFUNCTION()
		void HandleBeginOverlap(UPrimitiveComponent* OverlapComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult);

	UFUNCTION()
		void ToggleCollicion();
};
