// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeBaseElement;

UENUM()
enum class EMovementDirection 
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class MYPROJECT_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly);
	TSubclassOf<ASnakeBaseElement> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY(EditDefaultsOnly)
		float Speed;

	UPROPERTY()
		TArray<ASnakeBaseElement*> SnakeElements;

	UPROPERTY()
		EMovementDirection CurrentDirection;
		
	EMovementDirection LastDirection;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void Init(int ElementsNum);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement();

	void RemoveSnakeElement();

	void Move();

	UFUNCTION()
		void SnakeElementOverlap(ASnakeBaseElement* OverlapedElement,AActor* Other);
};
